FROM node:16 as prod
WORKDIR /get-availability
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "run", "prod" ]