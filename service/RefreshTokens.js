const nodeCron = require('node-cron');
var moment = require('moment');

var CacheMgr = require('./CacheMgr');
var Logger = require('./LoggingMgr').Logger();
var AuthMgr = require('./AuthMgr');

const job = nodeCron.schedule("0 55 * * * *", function refreshTokens() {
    Logger.info("Executing job to refresh cache");
    var accessToken = CacheMgr.GET("access_token");
    var isTTLLessThanConfig = false;
    if (accessToken) {
        var accessTokenStats = CacheMgr.TTL("access_token");
        Logger.info("Remaining ttl " + accessTokenStats);
        var remainingTtl = getRemainingCacheTime(accessTokenStats);
        Logger.info("Remaining ttl in seconds " + remainingTtl);
        isTTLLessThanConfig = remainingTtl <= (process.env.cache_job_duration);
    }
    if (!accessToken || isTTLLessThanConfig) {
        var refreshToken = CacheMgr.GET("refresh_token");
        Logger.info("Refresh Token " + refreshToken);
        if (refreshToken) {
            AuthMgr.getAuthTokenWithRefreshToken(refreshToken);
        } else {
            AuthMgr.getAuthTokenWithCreds();
        }
    } else {
       Logger.info("Found active access token skipping auth calls ");
    }
});

function getRemainingCacheTime(ttl) {
    var currentTime = moment();
    return currentTime.diff(ttl, 'seconds') * -1; 
}

function enableCacheRefresh() {
    Logger.info("Enabling job for cache refresh ");
    job.start();
    try {
        Logger.info("Fetch tokens after job configuration ");
        AuthMgr.getAuthTokenWithCreds();
    } catch (e) {
        Logger.error("Exception while fetching tokens after job configuration " + e.message);
    }
}

exports.enableCacheRefresh = enableCacheRefresh;  
