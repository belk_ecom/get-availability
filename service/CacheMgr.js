'use strict';
var AuthMgr = require('./AuthMgr');

const NodeCache = require('node-cache');
const Logger = require('./LoggingMgr').Logger();
const appCache = new NodeCache( {stdTTL : 10000, checkperiod: 3600});

function stats() {
    return appCache.getStats();
}

function getValue(key) {
    if (appCache.has(key)) {
        return appCache.get(key);
    }
    return null;
}

function putValue(key, value, ttl) {
    appCache.set(key, value, ttl);
}

function deleteKey(key) {
    appCache.del(key);
}

function flush() {
    appCache.flushAll();
}

function getStatsForKey(key) {
    return appCache.getTtl(key);
}

appCache.on( "expired", function(key, value) {
    Logger.info("Expiring key object " + key);
    if (key === 'access_token') {
        Logger.info("Refresh Auth token with refresh token ")
        AuthMgr.getAuthTokenWithRefreshToken();
    }
    if (key === 'refresh_token') {
        AuthMgr.getAuthTokenWithCreds();
    }
});

exports.GET = getValue;
exports.PUT = putValue;
exports.DELETE = deleteKey;
exports.FLUSH = flush;
exports.STATS = stats;
exports.TTL = getStatsForKey;


