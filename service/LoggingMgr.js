const { createLogger, format, transports } = require("winston");
var RotateFiles = require('winston-daily-rotate-file');
const { Console } = require("winston/lib/winston/transports")
var config = require("../config/config");

const logger = createLogger({
    level: config.app.logLevel,
    format: format.combine(format.timestamp(), format.json()),
    transports: [new RotateFiles({
      filename: 'logs/get-availability-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    }), new (Console)()],
});

function getLogger() {
    return logger;
}

exports.Logger = getLogger;