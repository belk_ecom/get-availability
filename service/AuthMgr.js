'use strict';

var querystring = require('node:querystring');
const fetch = require('node-fetch');

var CacheMgr = require('./CacheMgr');
var Logger = require('./LoggingMgr').Logger();

/**
 * 
 * @returns 
 */
async function getAuthTokenWithCreds() {
    var accessToken = null;
    var authparams = [process.env.auth_username, process.env.auth_password].join(":");
    try {
      Logger.info("Call auth with credentials");
      const response = await fetch(process.env.mao_auth_endpoint, {
        method: 'POST',
        headers: {
          "Host": process.env.host_name,
          "Authorization": "Basic " + Buffer.from(authparams).toString("base64"),
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: querystring.stringify({
          "grant_type": "password",
          "username": process.env.access_username,
          "password": process.env.access_password
        })
      });
      const authResponse = await response.json();
      Logger.info("response status for auth token call " + response.status);
      accessToken = getAccessToken(authResponse);
    } catch (e) {
      Logger.error("Exception while fetching auth token ", e.message);
    }
    return accessToken;
}

/**
 * 
 * @param {*} refreshToken 
 * @returns 
 */
async function getAuthTokenWithRefreshToken(refreshToken) {
  var refreshToken = CacheMgr.GET("refresh_token");
  var accessToken = null;
  if (!refreshToken) {
    Logger.info("Refresh token no longer valid, calling with credentials");
    accessToken = await getAuthTokenWithCreds();
  } else {
    var authparams = [process.env.auth_username, process.env.auth_password].join(":");
    try {
      Logger.info("Getting access token with refresh token ");
      const response = await fetch(process.env.mao_auth_endpoint, {
        method: 'POST',
        headers: {
          "Host": process.env.host_name,
          "Authorization": "Basic " + Buffer.from(authparams).toString("base64"),
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: querystring.stringify({
          "grant_type": "refresh_token",
          "refresh_token": refreshToken
        })
      });
      const authResponse = await response.json();
      Logger.info("response status for refresh token call " + response.status);
      if (response.status !== 200) {
         return getAuthTokenWithCreds();
      }
      accessToken = getAccessToken(authResponse);
    } catch (e) {
      Logger.error("Exception while fetching auth token with refresh ", e.message);
    }
  }
  return accessToken;
}

/**
 * 
 * @param {*} response 
 * @returns 
 */
function getAccessToken(response) {
    var accessToken = null;
    if (response) {
        CacheMgr.PUT("access_token", response.access_token, process.env.ttl_access_token);
        CacheMgr.PUT("refresh_token", response.refresh_token, process.env.ttl_refresh_token);
        accessToken = response.access_token;
    }
    return accessToken;
}

exports.getAuthTokenWithCreds = getAuthTokenWithCreds;
exports.getAuthTokenWithRefreshToken = getAuthTokenWithRefreshToken;