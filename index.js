const express = require('express');
const app = express();
const port = require('./config/config').app.port;

app.get('/', (req, res) => {
    res.contentType('text/html');
    res.send('Hello World');
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
    Logger.info("Enabling job for cache refresh ");
});