'use strict';

const dev = {
    app: {
        port: 3000,
        logLevel: "info",
        "mao" : {
            hostname: 'belkq.cga.omnib.manh.com',
            path: '/inventory/api/availability/location/availabilitydetail'
        },
        "dom" : {
            hostname: 'services-scm-qa.belk.com',
            path: '/services/atc/availability/getAvailabilityList?j_username=BelkDwre'
        }
        
    }
}

const test = {
    app: {
        port: 3000,
        logLevel: "info",
        "mao" : {
            hostname: 'services-scm-qa.belk.com',
            path: '/dwre/mao/inventory/availability'
        },
        "dom" : {
            hostname: 'services-scm-qa.belk.com',
            path: '/services/atc/availability/getAvailabilityList?j_username=BelkDwre'
        }
        
    }
}

const perf = {
    app: {
        port: 3001,
        logLevel: "error",
        "mao" : {
            hostname: 'services-scm-qa.belk.com',
            path: '/dwre/mao/inventory/availability'
        },
        "dom" : {
            hostname: 'services-scm-stg.belk.com',
            path: '/services/atc/availability/getAvailabilityList?j_username=BelkDwre'
        }
        
    }
}

const prod = {
    app: {
        port: 3000,
        logLevel: "error",
        "mao" : {
            hostname: 'services-scm.belk.com',
            path: '/dwre/mao/inventory/availability'
        },
        "dom" : {
            hostname: 'services-scm.belk.com',
            path: '/services/atc/availability/getAvailabilityList?j_username=BelkDwre'
        }
        
    }
}

const config = {
    dev, test, perf, prod
}

module.exports = config[process.env.NODE_ENV];