'use strict';

var express = require('express');
var app = express();
var cors = require('cors');
var https = require('https');
var router = express.Router();
var Logger = require('../service/LoggingMgr').Logger();
var CacheMgr = require("../service/CacheMgr");
var AuthMgr = require("../service/AuthMgr");

app.options('*', cors());

const corsheaders = (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Origin-Request-Header', '*');
	res.setHeader('content-type', 'application/json');
  return res.status(200).end();
}
const config = require('../config/config');

router.options('/inventory/mao', (req, res) => {
  corsheaders(req, res);
});

router.options('/inventory/dom', (req, res) => {
  corsheaders(req, res);
});

router.get('/inventory/mao', (req, res) => {
  corsheaders(req, res);
});

router.get('/inventory/dom', (req, res) => {
  corsheaders(req, res);
});

/**
 * Healthcheck Endpoint
 */
router.get('/', (req, res) => {
  res.status(200).end();
});

/**
 * MAO Endpoint
 */
router.post('/inventory/mao', function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Origin-Request-Header', '*');
	res.setHeader('content-type', 'application/json');
 
  var invRequest = {};
  invRequest.ViewName = "Store Inventory";
  var items = {}, stores = [];
  req.body.items.forEach((item) => {
    items[item.orin] = item.pid;
  });
  req.body.stores.forEach((store) => {
    stores.push(store.padStart(4, '0'));
  });
  invRequest.Items = Object.keys(items);
  invRequest.Location = stores;
  const headers = formatHeaders(req.headers);
  delete headers.cookie;
  delete headers.origin;
  var accessToken = CacheMgr.GET("access_token");
  if (accessToken == null) {
      AuthMgr.getAuthTokenWithRefreshToken();
      res.status(500).json({statusMsg : "Invalid access token, please try again later"});
      return;
  }
  headers['Authorization'] = "Bearer " + accessToken;
  headers['content-length'] = Buffer.byteLength(JSON.stringify(invRequest));
  var page = req.query.page;
  var options = {
    hostname: process.env.mao_inv_hostname,
    path: process.env.mao_inv_path,
    port: 443,
    method: 'POST',
    headers,
    timeout: parseInt(process.env.timeout)
  }
  const { method } = req;
  return new Promise((resolve, reject) => {
    let data = '';
    var svc = https.request(options, (invResp) => {
      invResp.setEncoding("utf-8");
      var body = [];
      invResp.on('data', d => {
        body.push(d);
      });
      invResp.on('end', () => {
        data = body.join('');
        var invData = {};
        if (data && invResp.statusCode === 200) {
          var inventory = {}, missingData = false;
          try {
            inventory = JSON.parse(data);
          } catch (e) {
            Logger.error("Error parsing service response: " + data);
          }
          if (inventory.data && inventory.data.length) {
            let parsedResponse = parseResponse(inventory.data, stores, {"item":"ItemId","store":"LocationId","stock":"Quantity"}, page);
            missingData = Object.keys(parsedResponse).length === 0;
            for (var orin in items) {
              invData[items[orin]] = parsedResponse[orin];
            }
          } else {
            missingData = true;
          }
          if (missingData) {
            invRequest.Items.forEach(function (item){
              Logger.info("item " + item);
              var storeInfo = {};
              stores.forEach(function(store){
                storeInfo[parseInt(store, 10)] = 0;
              });
              invData[items[item]] = storeInfo;
            });
          }
          
        } else {
          invData.statusCode = invResp.statusCode;
          invData.statusMsg = invResp.statusMessage;
        }
        if (!res.headersSent)
          res.status(invResp.statusCode).json(invData);
        resolve();
      })
    });
    svc.on('error', (error) => {
      Logger.error("Error while processing request " + error.message);
      if (!res.headersSent) {
        res.status(500).send(error);
      }
      reject();
    })
    svc.on('timeout', () => {
      if (!res.headersSent) {
        res.status(500).send("Request Timed out");
      }
      reject();
    });
    if (method == 'POST') {
      svc.write(JSON.stringify(invRequest));
    }
    svc.end();
  }).catch (function (err) {
    Logger.error("Handling errors for rejection of generic errors " + err);
  });
});

/**
 * DOM Endpoint
 */
router.post('/inventory/dom', (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Origin-Request-Header', '*');
	res.setHeader('content-type', 'application/json');
  var invRequest = {};
  var availabilityRequest = {
    "viewName": "store inventory",
    "availabilityCriteria" : {
      "itemNames": {
        "itemName": []
      },
      "facilityNames": {
        "facilityName": []
      }
    }
  };
  var items = [], stores = [];
  req.body.items.forEach((item) => {
    items.push(item.pid);
  });
  availabilityRequest.availabilityCriteria.itemNames.itemName = items;
  req.body.stores.forEach((store) => {
    stores.push(store);
  });
  availabilityRequest.availabilityCriteria.facilityNames.facilityName = stores;
  invRequest["availabilityRequest"] = availabilityRequest;
  let headers = formatHeaders(req.headers);
  delete headers.cookie
  headers['content-length'] = Buffer.byteLength(JSON.stringify(invRequest));
  Logger.info("Formatted Request " + JSON.stringify(invRequest));
  var options = {
    hostname: config.app['dom'].hostname,
    path: config.app['dom'].path,
    port: 443,
    method: 'POST',
    headers
  }
  return new Promise((resolve, reject) => {
    let data = '';
    var svc = https.request(options, (invResp) => {
      invResp.setEncoding("utf-8");
      var body = [];
      invResp.on('data', d => {
        body.push(d);
      });
      invResp.on('end', () => {
        data = body.join('');
        var invData = {};
        Logger.info("Service Response " + data);
        if (data && invResp.statusCode === 200) {
          var parsedData = {};
          try {
            parsedData = JSON.parse(data);
          } catch (e) {
            Logger.error("Error parsing service response: " + data);
          }
          if (parsedData.availability && parsedData.availability.availabilityDetails) {
            let svcResp = parseResponse(parsedData.availability.availabilityDetails.availabilityDetail, stores, 
                                        {"item": "itemName","store" : "facilityName", "stock": "atcQuantity"});
            invData[svcResp.item] = svcResp.stores;
          } else if (parsedData.availability && parsedData.availability.messages) {
            parsedData.availability.messages.message.forEach(msg => {
              Logger.info("No data for request with message " , msg);
              invData.statusCode = invResp.statusCode;
              invData.statusMsg = msg.description;
            });
          }
        } else {
          invData.statusCode = invResp.statusCode;
          invData.statusMsg = invResp.statusMessage;
        }
        res.status(invResp.statusCode).json(invData);
        resolve();
      })
    });
    svc.on('error', (error) => {
      reject(error.message);
    });
    svc.write(JSON.stringify(invRequest));
    svc.end();
  }).catch(error => {
    Logger.error("Request failed with code ", error);
  })
});

function formatHeaders(reqHeaders) {
  const removeHeaders = [
    'host',
    'user-agent',
    'accept-encoding'
  ]
  const headerKeys = Object.keys(reqHeaders)
  const headers = {};
  headerKeys.forEach((key) => {
    if (!removeHeaders.includes(key)) {
      headers[key] = reqHeaders[key]
    }
  })
  return headers;
}

function parseResponse(resBody, stores, keys, page) {
  let processedStores = {}, orinDetails = {};
  resBody.forEach(function(item){
    let storeId = item[keys.store];
    if (!stores.includes(storeId)) {
      return;
    }
    storeId = page ? storeId : parseInt(storeId, 10);
    if (!orinDetails.hasOwnProperty(item[keys.item])) {
      let store = {};
      store[storeId] = item[keys.stock];
    	orinDetails[item[keys.item]] = store;
      return;
    }
    var storeInfo = orinDetails[item[keys.item]];
    storeInfo[storeId] = item[keys.stock];
    orinDetails[item[keys.item]] = storeInfo;
  });
  Object.keys(orinDetails).forEach(function(orin) {
    processedStores[orin] = Object.keys(orinDetails[orin]).join(",");
  });
  stores.forEach(function (store) {
      Object.keys(processedStores).forEach(function (orin) {
        store = page ? store : parseInt(store, 10);
        if (processedStores[orin].indexOf(store) === -1) {
            var stores = orinDetails[orin];
            stores[store] = 0;
            orinDetails[orin] = stores;
        }
      });
  });
  return orinDetails;
}

module.exports = router;
