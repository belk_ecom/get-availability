var express = require('express');
var router = express.Router();
var AuthToken = require('../service/AuthMgr');

/* GET users listing. */
router.get('/', function(req, res, next) {
    var accessToken = AuthToken.getAuthTokenWithCreds();
    console.log("access token " + accessToken);
    res.status(200).json({accessToken : accessToken});
});

router.get('/refresh', function(req, res, next) {
    var CacheMgr = require("../service/CacheMgr");
    var accessToken = AuthToken.getAuthTokenWithRefreshToken(CacheMgr.GET("refresh_token"));
    console.log("access token " + accessToken);
    res.status(200).json({accessToken : accessToken});
});

module.exports = router;