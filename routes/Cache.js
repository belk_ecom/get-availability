var express = require('express');
var router = express.Router();
var CacheMgr = require('../service/CacheMgr');

router.get('/flush', function(req, res) {
   CacheMgr.FLUSH();
   res.status(200).end();
});

router.post('/clear/:key', function(req,res) {
    CacheMgr.DELETE(req.params.key);
    res.status(200).end();
});

router.get('/', function(req,res) {
    var stats = CacheMgr.STATS();
    res.status(200).json({stats : stats});
});

router.get('/get/:key', function(req, res){
    res.status(200).json({key: CacheMgr.GET(req.params.key)});
});

module.exports = router;